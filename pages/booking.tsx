import { useEffect, useState } from 'react';

import { NextPage } from "next";
import Booking from "../components/Booking";
// import ProtectPage from "../components/Protect";

import { useRouter } from 'next/router';
import { authVerify } from '../utils/authVerify';

const BookingDetailsPage: NextPage = (props: any) => {
  const [isValid, setValid] = useState(false);
  const router = useRouter();

  useEffect(() => {
    if(props.data){
      setValid(true);
    }else{
      router.push('/');
    }
    console.log(props.data);
    
  }, [isValid]);

  return (
    <>
      {isValid ? (<Booking />) : (<></>)}
    </>
  )
}

BookingDetailsPage.getInitialProps = async ctx => {
  const data = await authVerify(ctx);
  return { data };
}

// const BookingDetailsPage: NextPage = () => {
//   return (
//     <ProtectPage>
//       <Booking/>
//     </ProtectPage>
//   )
// }

export default BookingDetailsPage;