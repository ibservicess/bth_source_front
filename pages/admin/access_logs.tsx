import dynamic from 'next/dynamic'

const AccessLogsDYN = dynamic(
  () => import('../../components/Admin/AccessLogs'),
  { ssr: false }
)

const AccessLogsPage: React.FC = () => {
  return (
    <AccessLogsDYN/>
  )
}

export default AccessLogsPage;