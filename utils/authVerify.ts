import { NextPageContext } from "next";

export const authVerify = async (ctx: NextPageContext) => {  
  const res = await fetch('http://localhost:3333/auth/verify', {
    method: "POST",
    headers: {
      cookie: ctx.req?.headers.cookie
    }
  });

  const isAuth = res.status;
  console.log(isAuth);
  
  if(isAuth == 202){
    return true;
  }

  return false;
}