import React, { useEffect } from 'react';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { TablePagination, TableFooter, NoSsr } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { Container, Button, ButtonGroup } from '@material-ui/core';

import NavBarOld from '../../NavBar/NavBarOld';

import { StyledTableCell, StyledTableRow, useStyles } from './styles';
import { TablePaginationActions } from './handlers';
import api from '../../../services/api';
import { NextPage } from 'next';

function createData(id: number, userName: string, timestamp: string) {
  return { id, userName, timestamp };
}

const AccessLog: NextPage = ({pushNewRows}: any) => {
  const classes = useStyles();

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
  const [reload, setReload] = React.useState(0);

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleLoad = () => {
    setReload(reload+1);
  }

  useEffect(() => {
    api.get('logs/logins').then((response) => {
      const pushNewRows = rows;
      const data: Array<any> = response.data;
      const nwData = data.sort().reverse()
      nwData.map((info) => {
        let newName = info.name + ' ' + info.last_name;
        pushNewRows.push(createData(info.id, newName, info.logged_at));
      });
      
      setRows(pushNewRows);
    })
  
  }, [reload]);

  return (
    <div>
      <NoSsr>
      <NavBarOld/>
      <div className={classes.btnTable}>
        <ButtonGroup variant="contained" color="primary" aria-label="contained primary button group">
          <Button onClick={handleLoad}>Reload</Button>
        </ButtonGroup>
      </div>
      <Container className={classes.spaceTop} fixed>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="customized table">

            <TableHead>
              <TableRow>
                <StyledTableCell>ID</StyledTableCell>
                <StyledTableCell>User</StyledTableCell>
                <StyledTableCell align="right">Logged at</StyledTableCell>
              </TableRow>
            </TableHead>

            <TableBody>
              {(rowsPerPage > 0
                ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                : rows
              ).map((row) => (
                <StyledTableRow key={row.id + row.timestamp}>
                  <StyledTableCell component="th" scope="row">
                    {row.id}
                  </StyledTableCell>
                  <StyledTableCell>{row.userName}</StyledTableCell>
                  <StyledTableCell align="right">{row.timestamp}</StyledTableCell>
                </StyledTableRow>
              ))}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>

            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[10, 30, 50, { label: 'All', value: -1 }]}
                  colSpan={3}
                  count={rows.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    inputProps: { 'aria-label': 'rows per page' },
                    native: true,
                  }}
                  onChangePage={handleChangePage}
                  onChangeRowsPerPage={handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>

          </Table>
        </TableContainer>
      </Container>
      </NoSsr>
    </div>
  );
}

export default AccessLog;