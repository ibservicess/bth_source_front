import { useEffect, useState } from 'react';

import { useRouter } from 'next/router';
import { GetServerSideProps, NextPage } from 'next';
import { authVerify } from '../../utils/authVerify';

const Protect: NextPage = (props: any) => {
  const [isValid, setValid] = useState(false);
  const router = useRouter();

  useEffect(() => {
    if (props.data) {
      setValid(true);
    } else {
      router.push('/');
    }
    console.log(props.data);

  }, [isValid]);

  return (
    <>
      {isValid ? (props.children) : (<></>)}
    </>
  )
}

// Protect.getInitialProps = async ctx => {
//   const data = await authVerify(ctx);
//   return { data };
// }


// TEST
export const getServerSideProps: GetServerSideProps = async (ctx) => {
  return { props: { data: true } }
}

export default Protect;