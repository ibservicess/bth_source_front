import React, { FormEvent, useCallback, useEffect } from 'react';

import { Button, 
        createStyles, 
        Grid, 
        Link, 
        makeStyles, 
        NoSsr, 
        Paper, 
        TextField, 
        Theme } from '@material-ui/core';

import NavBarOld from '../NavBar/NavBarOld';
import api from '../../services/api';
import { Alert, AlertTitle } from '@material-ui/lab';

import InputMask from 'react-input-mask';
import { isValid, strip } from "@fnando/cnpj"; 
import { emailIsValid } from '../../utils/emailVerify';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: theme.spacing(1),
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
      },
      '& .MuiButton-root':{
        margin: theme.spacing(1),
      },
    },
    gridMargin: {
      marginTop: '20px',
    }
  }),
);

const CreateAccount: React.FC = () => {
  const classes = useStyles();

  /* NOTIFICATION */
  const [successRegister, setSuccessRegister] = React.useState(false);
  const [username, setUsername] = React.useState('');
  const [invalidForm, setInvalidForm] = React.useState(false);
  const [invalidNotification, setInvalidNotification] = React.useState(false);
  
  /* FORM */
  const [name, setName] = React.useState('');
  const [last_name, setLastName] = React.useState('');
  const [company_name, setCompanyName] = React.useState('');
  const [cnpj, setCnpj] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [zip_code, setZipCode] = React.useState('');
  const [address_name, setAddressName] = React.useState('');
  const [address_number, setAddressNumber] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [confirmPassword, setConfirmPassword] = React.useState('');

  /* ATTRIB */
  const [passwordError, setPasswordError] = React.useState({
    error: false
  });
  const [cnpjError, setCnpjError] = React.useState({
    error: false
  });
  const [emailError, setEmailError] = React.useState({
    error: false
  });

  const handleCreateAccount = (event: FormEvent) => {
    event.preventDefault();

    if(!invalidForm){
      api.post('users', {
        name,
        last_name,
        company_name,
        cnpj: strip(cnpj),
        email,
        zip_code,
        address_name,
        address_number,
        password
      }).then((response) => {
        setSuccessRegister(true);
        setUsername(response.data['username']);
      }).catch(() => {
  
      });
    }else{
      setInvalidNotification(true);
    }
  };

  useEffect(() => {
    if(password != confirmPassword){
      setPasswordError({error:true});
      setInvalidForm(true);
    }else{
      setPasswordError({error:false});
      setInvalidForm(false);
    }

    if(isValid(cnpj) || (cnpj == '')){
      setCnpjError({error:false});
      setInvalidForm(false);
    }else{
      setCnpjError({error:true});
      setInvalidForm(true);
    }

    if(emailIsValid(email) || email == ''){
      setEmailError({error:false});
      setInvalidForm(false);
    }else{
      setEmailError({error:true});
      setInvalidForm(true);
    }

  }, [confirmPassword, email, cnpj])

  return (
    <NoSsr>
      <NavBarOld/>
      <Grid container className={classes.gridMargin} alignItems="center" justify="center">
        <Paper variant="outlined">
        <Grid item sm={1} md={12}>
          { invalidNotification ? (
              <Alert severity="error">
                <AlertTitle>Attention!</AlertTitle>
                  Please, correct the information that is incorrect.
                </Alert>
            ):(<></>) }
          {successRegister ? (
                <Alert severity="success">
                <AlertTitle>Register success!</AlertTitle>
                  You can use your account — <Link href="/"><strong>Click here for login!</strong></Link>
                  <br/>
                  This is your username: <strong><h3>{username}</h3></strong>
                </Alert>
          ):(
          <form onSubmit={handleCreateAccount} className={classes.root} autoComplete="off">
            <Grid item>
                <TextField
                  id="name"
                  label="Your name"
                  variant="outlined"
                  required
                  onChange={(e) => {setName(e.target.value)}}
                  />
                <TextField
                  id="last_name"
                  label="Last name"
                  variant="outlined"
                  required
                  onChange={(e) => {setLastName(e.target.value)}}
                  />
            </Grid>
            <Grid item>
                <TextField
                  id="company_name"
                  label="Company name"
                  variant="outlined"
                  required
                  onChange={(e) => {setCompanyName(e.target.value)}}
                />
                <InputMask mask="99.999.999/9999-99" maskChar={null} onChange={(e) => {setCnpj(e.target.value)}}>
                {() => <TextField
                        id="cnpj"
                        label="CNPJ"
                        variant="outlined"
                        required 
                        {...cnpjError}
                      />}  
                </InputMask>
                  
            </Grid>
            <Grid item>
                <TextField
                  id="email"
                  type="email"
                  label="Your email"
                  variant="outlined"
                  required
                  {...emailError}
                  onChange={(e) => {setEmail(e.target.value)}}
                />
            </Grid>
            <Grid item>
              <TextField
                id="password"
                type="password"
                label="Your password"
                variant="outlined"
                required
                onChange={(e) => {setPassword(e.target.value)}}
              />
              <TextField
                id="confirmPassword"
                type="password"
                label="Confirm your password"
                variant="outlined"
                required
                {...passwordError}
                onChange={(e) => {setConfirmPassword(e.target.value)}}
              />
            </Grid>
            <Grid item>
                <TextField
                  id="zip_code"
                  label="Zip Code"
                  variant="outlined"
                  required
                  onChange={(e) => {setZipCode(e.target.value)}}
                />
                <Grid item>
                  <TextField
                    id="address_name"
                    label="Address name"
                    variant="outlined"
                    required
                    onChange={(e) => {setAddressName(e.target.value)}}
                  />
                  <TextField
                    id="address_number"
                    label="Address number"
                    variant="outlined"
                    required
                    onChange={(e) => {setAddressNumber(e.target.value)}}
                  />
                </Grid>
            </Grid>
            <Grid item>
              <Button type="submit" variant="contained" color="primary">
                Register
              </Button>
            </Grid>
          </form>
          )}
        </Grid>
        </Paper>
      </Grid>
    </NoSsr>
  );
}

export default CreateAccount;