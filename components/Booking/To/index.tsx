import React, { forwardRef, useImperativeHandle, useRef } from 'react';
import GoogleMaps, { IGoogleMapsPlacesHandle } from './Google';

const BookingTo: React.ForwardRefRenderFunction<IGoogleMapsPlacesHandle> = (props, ref) => {
  const googleLocaleRef = useRef<IGoogleMapsPlacesHandle>(null);
  
  useImperativeHandle(ref, () => {
    return { getValue };
  });
  const getValue = () => {
    return googleLocaleRef.current?.getValue();
  }

  return (
    <div>
      <h3>To</h3>
      <p className="mb-3"><b>Location</b></p>
      <div className="form-group">
          <GoogleMaps ref={googleLocaleRef}  id="booking_to"/>
      </div>
    </div>
  )
}

export default forwardRef(BookingTo);