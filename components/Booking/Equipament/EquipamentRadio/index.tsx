import React, { SelectHTMLAttributes } from 'react';

interface RadioProps extends SelectHTMLAttributes<HTMLSelectElement>{
    name?: string;
    label?: string;
    value?: string;
}

const EquipamentRadio: React.FC<RadioProps> = ({ name, label, value, onChange, required}) => {
    return (
        <div className="form-group">
          <input type="radio" id={label} name={name} value={value} {...onChange} {...required} />
          <label htmlFor={label}><strong>&nbsp;{label}</strong></label>
        </div>
    );
}

export default EquipamentRadio;