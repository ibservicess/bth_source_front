import React, { SelectHTMLAttributes, useEffect, useState } from 'react';

import EquipamentRadio from './EquipamentRadio'

interface BookEquipRadioProps extends SelectHTMLAttributes<HTMLSelectElement>{

}

const BookingEquipament: React.FC<BookEquipRadioProps> = ({...rest}) => {

  return (
    <div>
      <h3 className="">Equipament</h3>
          <EquipamentRadio name="equipament-radio" label="Reefer" value="reefer" required />
          <EquipamentRadio name="equipament-radio" label="Standard" value="standard" />
          <div className="form-group">
            <small className="form-text text-muted text-right"></small>
          </div>
    </div>
  )
}

export default BookingEquipament;