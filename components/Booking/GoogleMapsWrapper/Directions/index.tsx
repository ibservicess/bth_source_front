import { Polyline, Marker, Map } from "google-maps-react"
import { forwardRef, useEffect, useImperativeHandle, useState } from "react";
import api from "../../../../services/api";

interface IGoogleDirections {
  place_id: {
    from: string;
    to: string;
  };
  origin: string;
  destination: string;
  google?: any;
  containerStyle?: any;
}

export interface IGoogleDirectionsHandle {
  points: any[];
  fetchPolylines: () => void;
  fetchPlaces: () => void;
}

const GoogleDirections: React.ForwardRefRenderFunction<IGoogleDirectionsHandle, IGoogleDirections> = (props, ref) => {
  const [points, setPoints] = useState([]);
  const [poly, setPoly] = useState([]);
  const [loaded, setLoaded] = useState(false);

  // Create a polyline for routes
  const fetchPolylines = () => {
    api.get(`/geo/routes?origin=${props.origin}&destination=${props.destination}`)
      .then(response => {
        console.log("API response:", response);
        setPoints(response.data['points']);

      });
  }

  // Fetch Geolocation between address
  const fetchPlaces = () => {
    api.get(`/geo/unique?place_id=${props.place_id.from}`)
      .then(response => {
        console.log("Places From response:", response);
        const newPoints = points;

        newPoints.push(response.data['geometry']['location']);
      })

    api.get(`/geo/unique?place_id=${props.place_id.to}`)
      .then(response => {
        console.log("Places To response:", response);
        const newPoints = points;

        newPoints.push(response.data['geometry']['location']);
      })

    console.log("Pontooos", points)

    createPolylineTwoPoints();
  }

  // Create a line of polyline by From and To
  const createPolylineTwoPoints = () => {
    const newPoly = [];

    newPoly.push(points[0]);
    newPoly.push(points[points.length - 1])
    setPoly(newPoly);
    console.log("Poly", poly);
  }

  useImperativeHandle(ref, () => {
    return { points, fetchPolylines, fetchPlaces };
  });

  useEffect(() => {
    fetchPlaces()
  }, [loaded, props.place_id])

  if (!loaded) {
    // fetchPolylines();
    // fetchPlaces();
    setLoaded(true);

  }

  if (poly == undefined || poly == null) {
    createPolylineTwoPoints();
    setLoaded(false);
    return <div>Loading...</div>
  }

  return (
    <>
      {!loaded
        ? (<></>)
        : (
          <Map
            containerStyle={props.containerStyle}
            google={props.google}
            initialCenter={points[points.length - 1]}
            center={points[points.length - 1]}
          >
            <Marker
              title={'From...'}
              name={'From'}
              position={points[0]}
            >

            </Marker>
            <Marker
              title={'...To'}
              name={'To'}
              position={points[points.length - 1]}
            >

            </Marker>

            <Polyline
              path={poly}
              strokeColor="#0000FF"
              strokeOpacity={0.8}
              strokeWeight={4}
            />

          </Map>
        )
      }
    </>
  )
}

export default forwardRef(GoogleDirections);