import { forwardRef, useImperativeHandle, useState } from 'react';
import GoogleDirections from './Directions';

const containerStyle = {
  position: 'relative',
  width: '100%',
  height: '50vh'
}

interface IMapContainerProps {
  place_id: {
    from: string;
    to: string;
  };
  origin: string;
  destination: string;
  children?: Element | any;
}

export interface IMapContainerHandler {
  loadGoogleApi: () => void;
}

const MapContainer: React.ForwardRefRenderFunction<IMapContainerHandler, IMapContainerProps> = (props, ref) => {
  const [googleApi, setGoogleApi] = useState('');

  useImperativeHandle(ref, () => {
    return { loadGoogleApi };
  });

  const loadGoogleApi = () => {
    setGoogleApi(window['google']);
  }

  if (!googleApi) {
    return (<div>Waiting for locale...</div>)
  }

  return (
    <>
      <GoogleDirections
        place_id={props.place_id}
        origin={props.origin}
        destination={props.destination}
        google={googleApi}
        containerStyle={containerStyle}
      />
      {props.children}
    </>
  )
}

export default forwardRef(MapContainer);