import React, { InputHTMLAttributes } from 'react';

interface InputProps extends InputHTMLAttributes<HTMLInputElement>{
    name: string;
    label?: string;
}

const Input: React.FC<InputProps> = ({ label, name, ...rest}) => {
  if(label){
    return (
      <div className="form-group">
          <input type="text" className="form-control" id={name} {...rest}/>
      </div>
    );
  }

  return (
        <div className="form-group">
            <label htmlFor={name}>{label}</label>
            <input type="text" className="form-control" id={name} {...rest}/>
        </div>
    );
}

export default Input;