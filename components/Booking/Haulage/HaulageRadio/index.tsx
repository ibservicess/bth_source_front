import React, { SelectHTMLAttributes } from 'react';

interface RadioProps extends SelectHTMLAttributes<HTMLSelectElement>{
    name: string;
    label: string;
}

const HaulageRadio: React.FC<RadioProps> = ({ name, label, ...rest}) => {
    return (
        <div className="form-group">
          <input type="radio" id={label} name={name} value="op1"/>
          <label htmlFor={label}><strong>&nbsp;{label}</strong></label>
        </div>
    );
}

export default HaulageRadio;