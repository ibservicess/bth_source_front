import HaulageRadio from './HaulageRadio';

const BookingHaulage: React.FC = () => {
  return (
    <div>
      <h3 className="">Haulage</h3>
          <HaulageRadio name="haulage-radio" label="Received at terminal / Ramp"/>
          <HaulageRadio name="haulage-radio" label="Received at your door"/>
          <HaulageRadio name="haulage-radio" label="Delivered at terminal / Ramp"/>
          <HaulageRadio name="haulage-radio" label="Delivered at you door"/>
          <div className="form-group">
            <small className="form-text text-muted text-right"></small>
          </div>
    </div>
  )
}

export default BookingHaulage;