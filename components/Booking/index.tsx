import NavBar from "../NavBar";
import NavBarOld from "../NavBar/NavBarOld";
import { Container, createStyles, makeStyles, Theme, Grid, Paper, Button, Divider } from "@material-ui/core";
import BookingFrom from "./From";
import BookingTo from "./To";
import BookingHaulage from "./Haulage";
import BookingEquipament from "./Equipament";
import EquipamentRadio from './Equipament/EquipamentRadio';
import MapContainer, { IMapContainerHandler } from './GoogleMapsWrapper';
import { useCallback, useEffect, useRef, useState } from "react";
import { IGoogleMapsPlacesHandle } from "./To/Google";
import { IGoogleDirectionsHandle } from "./GoogleMapsWrapper/Directions";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    spaceTop: {
      paddingTop: '20px'
    },
    paperPadding: {
      padding: '20px'
    }
  }),
);

const Booking: React.FC = () => {
  const classes = useStyles();
  const [placesIds, setPlacesIds] = useState({ from: '', to: '' });
  const [test, setTest] = useState('');

  const [from, setFrom] = useState('');
  const [to, setTo] = useState('');

  const googleFromLocale = useRef<IGoogleMapsPlacesHandle>(null);
  const googleToLocale = useRef<IGoogleMapsPlacesHandle>(null);

  const googleMaps = useRef<IMapContainerHandler>(null);
  const googleMapsRefresh = useRef<IGoogleDirectionsHandle>(null);

  const requestReloadMapHandle = useCallback(() => {
    console.log("From:", googleFromLocale.current?.getValue(), "To:", googleToLocale.current?.getValue());

    if ((googleFromLocale.current?.getValue() != null) && (googleToLocale.current?.getValue() != null)) {
      setPlacesIds({
        from: googleFromLocale.current?.getValue().place_id,
        to: googleToLocale.current?.getValue().place_id
      });

      setFrom(googleFromLocale.current?.getValue().description);
      setTo(googleToLocale.current?.getValue().description);

      googleMaps.current?.loadGoogleApi();

      // googleMapsRefresh.current?.fetchPlaces();
    }
  }, []);

  const requestBookingHandle = useCallback(() => {
    // prepare to send for back-end
  }, []);

  return (
    <div>
      <NavBarOld />
      <Container className={classes.spaceTop} fixed>
        <h2>Booking details</h2>
        <Paper className={classes.paperPadding} variant="outlined">
          <Grid container spacing={1}>
            <Grid item sm={6} >
              <BookingFrom ref={googleFromLocale} />
              <Divider variant="middle" component="div" />
            </Grid>
            <Grid item sm={6} >
              <BookingTo ref={googleToLocale} />
              <Divider variant="middle" component="div" />
            </Grid>
            <Grid item sm={6} >
              <BookingHaulage />
              <Divider variant="middle" component="div" />
            </Grid>
            <Grid item sm={6} >
              <div>
                <h3 className="">Equipament</h3>
                <EquipamentRadio name="equipament-radio" label="Reefer" value="reefer" onChange={(e) => setTest(e.target.value)} />
                <EquipamentRadio name="equipament-radio" label="Standard" value="standard" onChange={(e) => setTest(e.target.value)} />
                <div className="form-group">
                  <small className="form-text text-muted text-right"></small>
                </div>
              </div>
            </Grid>
            <Grid item sm={12}>
              <MapContainer
                ref={googleMaps}
                place_id={placesIds}
                origin={from}
                destination={to}
              >
                <Button variant="contained" onClick={requestReloadMapHandle} color="secondary">
                  Refresh
                </Button>
              </MapContainer>
            </Grid>
          </Grid>
          <br />
          <Button variant="contained" onClick={requestReloadMapHandle} color="primary">
            Request
          </Button>
        </Paper>
        <br />
      </Container>
    </div>
  )
}

export default Booking;