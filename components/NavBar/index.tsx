import {
    AppBar,
    Toolbar,
    Typography,
    IconButton,
    Button,
    makeStyles,
    createStyles,
    Theme
  } from '@material-ui/core';

import { Menu } from '@material-ui/icons';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      backgroundColor: '#f96332',
      boxShadow: '0px 0px'
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    contactBtn: {
      fontSize: '12px',
      padding: '10px',
      backgroundColor: 'white',
      borderRadius: '5px',
    },
    altBtn: {
      color: '#EEEEEE',
      fontSize: '10px',
      padding: '10px 5px 10px 5px'
    }
  }),
);

const NavBar: React.FC = () => {
  const classes = useStyles();

  return (
    <AppBar className={classes.root} position="static">
      <Toolbar>
        <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
          <Menu/>
        </IconButton>
        <Typography variant="h6" className={classes.title}>
          BRAND
        </Typography>
        <Button className={classes.altBtn} color="default" >Features</Button>
        <Button className={classes.altBtn} color="default" >Pricing</Button>
        <Button className={classes.altBtn} color="default" >About us</Button>
        <Button className={classes.altBtn} color="default" >FAQ</Button>
        <Button className={classes.contactBtn} color="default" >Contact us</Button>
      </Toolbar>
    </AppBar>
  )
}

export default NavBar;