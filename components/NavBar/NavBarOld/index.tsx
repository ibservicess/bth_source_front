const NavBarOld: React.FC = () => {

  return (
    <nav className="navbar navbar-expand-md navbar-dark bg-primary">
    <div className="container"> <a className="navbar-brand" href="/">
        <i className="fa d-inline fa-lg fa-stop-circle"></i>
        <b> BRAND</b>
      </a> <button className="navbar-toggler navbar-toggler-right border-0" type="button" data-toggle="collapse" data-target="#navbar16">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbar16">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item"> <a className="nav-link" href="#">Features</a> </li>
          <li className="nav-item"> <a className="nav-link" href="#">Pricing</a> </li>
          <li className="nav-item"> <a className="nav-link" href="#">About</a> </li>
          <li className="nav-item"> <a className="nav-link" href="#">FAQ</a> </li>
        </ul> <a className="btn navbar-btn ml-md-2 btn-light text-dark">Contact us</a>
      </div>
    </div>
    </nav>
  )

}

export default NavBarOld;