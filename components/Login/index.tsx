import React from 'react';

import Head from 'next/head';
import Link from 'next/link';

import { TextField, Paper, Button, NoSsr, Divider, Grid } from "@material-ui/core";
import useStyles from './styles';
import api from '../../services/api';
import { useRouter } from 'next/router';
import { Alert } from '@material-ui/lab';

import cookieCutter from 'cookie-cutter'

const Login: React.FC = () => {
  const classes = useStyles();
  const router = useRouter();

  const [username, setUsername] = React.useState('');
  const [password, setPasword] = React.useState('');
  const [alert, setAlert] = React.useState(false);

  const handleLogin = () => {
    api.post('auth/login', {
      username,
      password
    })
    .then((response) => {
      cookieCutter.set('auth', response.data['token'])
      window.location.href = '/booking';
      //router.push('/booking');
    })
    .catch(() => {
      setAlert(true);
    });
  }

  return (
    <NoSsr>
      <div className={classes.root}>
      <Head>
        <title>Login - Booking</title>
      </Head>
      <form className={classes.inTheMiddle} noValidate autoComplete="on">
        <Paper className={classes.paper} elevation={3}>
          <div>
            <h3>Login</h3>
            {alert ? (
              <>
                <Alert severity="error">
                  Maybe your <strong>Username</strong> or <strong>Password</strong> is wrong...
                </Alert>
                <br/>
              </>
            ):(<></>)}
            <TextField 
              id="bookingUsername" 
              label="Username" 
              variant="outlined"
              fullWidth
              onChange={(e) => {setUsername(e.target.value)}}
            />
            <br/>
            <TextField 
              id="bookingPass" 
              type="password" 
              label="Password" 
              variant="outlined"
              fullWidth
              onChange={(e) => {setPasword(e.target.value)}}
            />
            <br/>
            <span>
              <Link href="/forgot" >Forgot your password?</Link>
              <br/>
            </span>
            <br/>
            <Button className={classes.btnSize} variant="contained" 
                    color="primary" onClick={handleLogin}>
              LOGIN
            </Button>
          </div>
          <br/>
          <Divider/>
          <br/>
          <Grid container alignItems="center" justify="center">
            <strong>
              <Link href="/create-account">Create your account</Link>
            </strong>
          </Grid>
          <br/>
        </Paper>
      </form>
      </div>
    </NoSsr>
  )
}

export default Login;