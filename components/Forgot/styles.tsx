import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

const useForgotStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: '100vh',
      width: '100vw',
      backgroundColor: 'black',
      backgroundImage: 'url(/img/login.jpg)',
      backgroundBlendMode: 'luminosity',
      backgroundPosition: 'center',
      '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
      '& form': {
        height: '100vh'
      }
    },
    paper: {
      padding: '14px',
    },
    inTheMiddle: {
      height: '100vh',
      width: '100vw',
      padding: '0',
      margin: '0',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& .MuiFormControl-root':{
        margin: '0px 0px 12px 0px'
      }
    },
    btnSize: {
      backgroundColor: 'orange',
      width: '100%',
      '&:hover':{
        backgroundColor: '#f96332'
      }
    }
  }),
);

export default useForgotStyles;