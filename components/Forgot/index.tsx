import React from 'react';

import Head from 'next/head';
import Link from 'next/link';

import { TextField, Paper, Button, NoSsr, Divider, Grid } from "@material-ui/core";
import api from '../../services/api';
import { useRouter } from 'next/router';
import { Alert } from '@material-ui/lab';
import useForgotStyles from './styles';

import { NextPage } from 'next';

const Forgot: NextPage = () => {
  const classes = useForgotStyles();
  const router = useRouter();

  const [username, setUsername] = React.useState('');
  const [alert, setAlert] = React.useState(false);

  const handleUsername = () => {
    api.post('forgot/solicit', {
      username
    })
      .then((response) => {
        setAlert(true);
      })
      .catch(() => {
        setAlert(true);
      });
  }

  return (
    <NoSsr>
      <div className={classes.root}>
        <Head>
          <title>Forgot my password - Booking</title>
        </Head>
        <form className={classes.inTheMiddle} noValidate autoComplete="on">
          <Paper className={classes.paper} elevation={3}>
            <div>
              <h3>Forgot my password</h3>
              {alert ? (
                <>
                  <Alert severity="success">
                    If this <strong>username</strong> exists we a send a <strong>token</strong> to you on your <strong>email</strong>.
                  <br />
                  Please, check in you <strong>spam box</strong>.
                </Alert>
                  <br />
                </>
              ) : (<>
                <p>Insert your username, if exists we send an email to you.</p>
                <TextField
                  id="bookingUsername"
                  label="Username"
                  variant="outlined"
                  fullWidth
                  onChange={(e) => { setUsername(e.target.value) }}
                />

                <br />
                <Button className={classes.btnSize} variant="contained"
                  color="primary" onClick={handleUsername}>
                  Confirm
            </Button>
              </>)}
            </div>
            <br />
            <Divider />
            <br />
            <Grid container alignItems="center" justify="center">
              <strong>
                <Link href="forgot/token">I have a token</Link>
              </strong>
            </Grid>
            <br />
          </Paper>
        </form>
      </div>
    </NoSsr>
  )
}

export default Forgot;