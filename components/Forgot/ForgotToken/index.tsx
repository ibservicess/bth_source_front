import React, { useEffect } from 'react';

import Head from 'next/head';
import Link from 'next/link';

import { TextField, Paper, Button, NoSsr, Divider, Grid } from "@material-ui/core";
import api from '../../../services/api';
import { useRouter } from 'next/router';
import { Alert } from '@material-ui/lab';
import useForgotStyles from './../styles';

import { NextPage } from 'next';

const ForgotToken: NextPage = () => {
  const classes = useForgotStyles();
  const router = useRouter();
  const { tkn } = router.query;

  const [token, setToken] = React.useState('');
  const [alert, setAlert] = React.useState(false);

  const [tokenSession, setTokenSession] = React.useState(true);
  const [passwordSession, setPasswordSession] = React.useState(false);

  const [password, setPassword] = React.useState('');
  const [confirmPassword, setConfirmPassword] = React.useState('');
  const [matchPassword, setMatchPassword] = React.useState(false);

  const handleToken = () => {
    api.post('forgot/validate', {
      token
    })
      .then((response) => {
        if (response.status < 400) {
          setAlert(false);
          setTokenSession(false);
          setPasswordSession(true);
        } else {
          setAlert(true);
        }
      })
      .catch(() => {
        setAlert(true);
      });
  }

  const handlePassword = () => {
    if (matchPassword) {
      if (!(password == '' || confirmPassword == '')) {
        api.post('forgot/reset', {
          token,
          password
        })
          .then((response) => {
            if (response.status < 400) {
              setAlert(false);
              setPasswordSession(true);
              window.location.href = '/';
            } else {
              setAlert(true);
            }
          })
          .catch(() => {
            setAlert(true);
          });
      } else {
        setMatchPassword(false);
      }
    }
  }

  useEffect(() => {
    if (tkn) {
      setToken(tkn.toString());
      handleToken();
    }


    if (password == '' || confirmPassword == '') {
      setMatchPassword(true);
    } else if ((password != confirmPassword)) {
      setMatchPassword(false);
    } else {
      setMatchPassword(true);
    }
  }, [password, confirmPassword]);

  return (
    <NoSsr>
      <div className={classes.root}>
        <Head>
          <title>Token for new password - Booking</title>
        </Head>
        <form className={classes.inTheMiddle} noValidate autoComplete="on">
          <Paper className={classes.paper} elevation={3}>
            <div>
              {tokenSession ? (<>
                <h3>Token for new password</h3>
                {alert ? (
                  <>
                    <Alert severity="error">
                      Maybe your <strong>token</strong> is wrong.
                  <br />
                    </Alert>
                    <br />
                  </>
                ) : (<></>)}
                <p>Insert a valid token.</p>
                <TextField
                  id="bookingUserToken"
                  label="Token"
                  variant="outlined"
                  fullWidth
                  onChange={(e) => { setToken(e.target.value) }}
                />

                <br />
                <Button className={classes.btnSize} variant="contained"
                  color="primary" onClick={handleToken}>
                  Confirm
            </Button>
              </>) : (<></>)}

              {passwordSession ? (<>
                <h3>Create a new Password</h3>
                {!matchPassword ? (
                  <>
                    <Alert severity="error">
                      <strong>Password</strong> and <strong>confirm password</strong> does not match!
                  <br />
                    </Alert>
                    <br />
                  </>
                ) : (<></>)}
                <TextField
                  type="password"
                  id="bookingPassword"
                  label="New Password"
                  variant="outlined"
                  fullWidth
                  onChange={(e) => { setPassword(e.target.value) }}
                />
                <TextField
                  type="password"
                  id="bookingNewPassword"
                  label="Confirm Password"
                  variant="outlined"
                  fullWidth
                  onChange={(e) => { setConfirmPassword(e.target.value) }}
                />

                <br />
                <Button className={classes.btnSize} variant="contained"
                  color="primary" onClick={handlePassword}>
                  Create
            </Button>
              </>) : (<></>)}
            </div>
            <br />
          </Paper>
        </form>
      </div>
    </NoSsr>
  )
}

export default ForgotToken;